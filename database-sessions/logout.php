<?php
 session_start();
?>

<!DOCTYPE html>
<html>
    <head>
        <h1>Log Out</h1>
    </head>
    <body>
        <p>You have been logged out.</p>
        <a href="index.php">return to the cars page</a>
    </body>
</html>
<?php
$_SESSION = array();
session_destroy();

?>