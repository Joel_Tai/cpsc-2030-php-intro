<?php
 session_start();
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Cars - CPSC 2030</title>
    <?php if ( $_SESSION["loggedin"]==false ) { ?>
        <form action="https://2c42cb055ee044f6a7c5192a42912b9a.vfs.cloud9.us-east-1.amazonaws.com/cpsc-2030-php-intro/database-sessions/login.php">
          <div align="right">
            <input type="submit" value="login" />
          </div>
        </form>
    <?php } ?>
    
    <?php if ( $_SESSION["loggedin"] ) { ?>
        <form action="logout.php">
          <div align="right">
            <input type="submit" value="log out" />
          </div>
        </form>
    <?php } ?>
    
  </head>
  <body class="container">
    <h1>Cars - CPSC 2030</h1>
<?php if ( $_SESSION["loggedin"] ) { ?>
   
   <form class="needs-validation" novalidate method="POST" action="">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="make">Make</label>
            <input type="text" class="form-control" id="make" placeholder="" value="<?PHP echo $_SESSION["error1"]; ?>" required name="make">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="model">Model</label>
            <input type="text" class="form-control" id="model" placeholder=""value="<?PHP echo $_SESSION["error2"]; ?>" required name="model">
            <div class="invalid-feedback">
              Valid model is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="year">Year</label>
            <input type="number" class="form-control" id="year" placeholder="" value="" required name="year">
            <div class="invalid-feedback">
              Valid year is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="mileage">Mileage</label>
            <input type="number" class="form-control" id="mileage" placeholder="" value=""  required name="mileage">
            <div class="invalid-feedback">
              Valid mileage is required.
            </div>
          </div>
        </div>
        <button class="btn btn-primary btn-lg btn-block" type="submit">Add car</button>
        
        <input type="hidden" name="operation" value = "add"/>
    </form>
    
    
<?php } ?>
   

    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Make</th>
                <th scope="col">Model</th>
                <th scope="col">Year</th>
                <th scope="col">Mileage</th>
                <?php if ( $_SESSION["loggedin"] ) { ?>
                <th scope="col">Delete</th>
                 <?php } ?>
            </tr>
        </thead>
        <tbody>
            

        <?php
            $link = mysqli_connect( 'localhost', 'root', 'killdude1' );
            mysqli_select_db( $link, 'Demo' );
            if(isset($_REQUEST["make"]))
            {
                $safe_make = mysqli_real_escape_string( $link, $_REQUEST["make"] );
                $safe_model = mysqli_real_escape_string( $link, $_REQUEST["model"] );
                $safe_year = mysqli_real_escape_string( $link, $_REQUEST["year"] );
                $safe_mileage = mysqli_real_escape_string( $link, $_REQUEST["mileage"] );
                $query = "INSERT INTO cars ( Make, Model, Year, Mileage) VALUES ( '$safe_make', '$safe_model', '$safe_year', '$safe_mileage')";
                mysqli_query( $link, $query );
  
                
              
            }
        
              if($_REQUEST["operation"]=='delete')
              {
                $deleteID = $_REQUEST["ID"];
                $query = "DELETE FROM cars WHERE ID =  $deleteID";
                mysqli_query( $link, $query );
              }
          
              
            $results = mysqli_query( $link, 'SELECT * FROM cars' );
            // process $results
            while( $record = mysqli_fetch_assoc( $results ) ) 
            {
	            $ID = $record['ID'];
	            $make = $record['Make'];
            	$model = $record['Model'];
            	$year = $record['Year'];
            	$mileage = $record['Mileage'];
            	if($_SESSION["loggedin"])
            	{
            	  print " <form novalidate method=\"POST\" action=\"https://2c42cb055ee044f6a7c5192a42912b9a.vfs.cloud9.us-east-1.amazonaws.com/cpsc-2030-php-intro/database-sessions/index.php\"> <input type=\"hidden\" name=\"operation\" value = \"delete\"/> <input type=\"hidden\" name=\"ID\" value = \"$ID\"/> <tr> <td>$ID</td> <td>$make</td> <td>$model</td><td>$year</td><td>$mileage</td><td><button class=\"btn btn-danger\" type =\"submit\">Delete</button></td></tr></form>";
            	}
            	else
            	{
            	   print " <form novalidate method=\"POST\" action=\"https://2c42cb055ee044f6a7c5192a42912b9a.vfs.cloud9.us-east-1.amazonaws.com/cpsc-2030-php-intro/database-sessions/index.php\"> <input type=\"hidden\" name=\"operation\" value = \"delete\"/> <input type=\"hidden\" name=\"ID\" value = \"$ID\"/> <tr> <td>$ID</td> <td>$make</td> <td>$model</td><td>$year</td><td>$mileage</td></tr></form>";
            	}
            }
            mysqli_free_result( $results );
            mysqli_close( $link );
        ?>

        </tbody>
    </table>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
   
  </body>
</html>
