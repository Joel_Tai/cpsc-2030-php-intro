<?php
 session_start();
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel = "stylesheet" href="menu.css">
    <title>Shoes List</title>
    </head>
    <body class="container">
    <header>
    <?php if ( $_SESSION["loggedin"]==false ) { ?>
    
       <div class="dropdown">
        
          <button onclick="myFunction()" class="dropbtn">Menu</button>
          <nav>
            <div id="myDropdown" class="dropdown-content">
              <a href="login.php">Login</a>
              <a href="documentation.php">Documentation</a>
              <a href="shop.php">Gallary</a>
            </div>
          </nav>  
          
        </div>
        
  
    <?php } ?>
    
    <?php if ( $_SESSION["loggedin"] ) { ?>

       <div class="dropdown">
        
          <button onclick="myFunction()" class="dropbtn">Menu</button>
          <nav>
            <div id="myDropdown" class="dropdown-content">
              <a href="logout.php">Logout</a>
              <a href="documentation.php">Documentation</a>
              <a href="shop.php">Gallary</a>
            </div>
          </nav>  
          
        </div>
        

    <?php } ?>
    
 </header>
  
    <h1>Shoes List</h1>
<?php if ( $_SESSION["loggedin"] ) { ?>
   
   <form class="needs-validation" novalidate method="POST" action="index.php">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="company">Company</label>
            <input type="text" class="form-control" id="company" placeholder="" value="" required name="company">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="model">Model</label>
            <input type="text" class="form-control" id="model" placeholder="" value="" required name="model">
            <div class="invalid-feedback">
              Valid model is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="condition">Condition(/10)</label>
            <input type="number" class="form-control" id="condition" placeholder="" value="" required name="condition">
            <div class="invalid-feedback">
              Valid  is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="value">Value</label>
            <input type="number" class="form-control" id="value" placeholder="" value=""  required name="value">
            <div class="invalid-feedback">
              Valid value is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="color">Color</label>
            <input type="text" class="form-control" id="color" placeholder="" value="" required name="color">
            <div class="invalid-feedback">
              Valid model is required.
            </div>
          </div>
        </div>
        <button class="btn btn-primary btn-lg btn-block" type="submit">Add Shoe</button>
        
        <input type="hidden" name="operation" value = "add"/>
    </form>
    
    
<?php } ?>
   

    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Company</th>
                <th scope="col">Model</th>
                <th scope="col">Condition</th>
                <th scope="col">Value</th>
                <th scope="col">Color</th>
                <?php if ( $_SESSION["loggedin"] ) { ?>
                <th scope="col">Delete</th>
                 <?php } ?>
            </tr>
        </thead>
        <tbody>
            

        <?php
            $link = mysqli_connect( 'localhost', 'root', 'killdude1' );
            mysqli_select_db( $link, 'Demo' );
            
            if(isset($_REQUEST["company"]))
            {
                if( $_REQUEST["condition"]<=10)
                {
                  $safe_company = mysqli_real_escape_string( $link, $_REQUEST["company"] );
                  $safe_model = mysqli_real_escape_string( $link, $_REQUEST["model"] );
                  $safe_condition = mysqli_real_escape_string( $link, $_REQUEST["condition"] );
                  $safe_value = mysqli_real_escape_string( $link, $_REQUEST["value"] );
                  $safe_color = mysqli_real_escape_string( $link, $_REQUEST["color"] );
                  $query = "INSERT INTO shoes ( Company, Model, Status, Value, Color) VALUES ( '$safe_company', '$safe_model', '$safe_condition', '$safe_value', '$safe_color')";
                  mysqli_query( $link, $query );
                  }
                
              
            }
        
              if($_REQUEST["operation"]=='delete')
              {
                $deleteID = $_REQUEST["ID"];
                $query = "DELETE FROM shoes WHERE ID =  $deleteID";
                mysqli_query( $link, $query );
              }
          
              
            $results = mysqli_query( $link, 'SELECT * FROM shoes' );
            // process $results
            while( $record = mysqli_fetch_assoc( $results ) ) 
            {
	            $ID = $record['ID'];
	            $company = $record['Company'];
            	$model = $record['Model'];
            	$condition = $record['Status'];
            	$value = $record['Value'];
            	$color = $record['Color'];
            	if($_SESSION["loggedin"])
            	{
            	  print " <form novalidate method=\"POST\" action=\"https://2c42cb055ee044f6a7c5192a42912b9a.vfs.cloud9.us-east-1.amazonaws.com/cpsc-2030-php-intro/project/index.php\"> <input type=\"hidden\" name=\"operation\" value = \"delete\"/> <input type=\"hidden\" name=\"ID\" value = \"$ID\"/> <tr> <td>$ID</td> <td>$company</td> <td>$model</td><td>$condition/10</td><td>$$value</td><td>$color</td><td><button class=\"btn btn-danger\" type =\"submit\">Delete</button></td></tr></form>";
            	}
            	else
            	{
            	   print " <form novalidate method=\"POST\" action=\"https://2c42cb055ee044f6a7c5192a42912b9a.vfs.cloud9.us-east-1.amazonaws.com/cpsc-2030-php-intro/project/index.php\"> <input type=\"hidden\" name=\"operation\" value = \"delete\"/> <input type=\"hidden\" name=\"ID\" value = \"$ID\"/> <tr> <td>$ID</td> <td>$company</td> <td>$model</td><td>$condition</td><td>$value</td><td>$color</td></tr></form>";
            	}
            }
            mysqli_free_result( $results );
            mysqli_close( $link );
        ?>

        </tbody>
    </table>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src = "menu.js"></script>
   
  </body>
</html>
