<?php
 session_start();
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8"> 
    <title>Documentation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel = "stylesheet" href="menu.css">
   
  </head>
  <body class="container">
  <header>
     <?php if ( $_SESSION["loggedin"]==false ) { ?>
  
       <div class="dropdown">
        
          <button onclick="myFunction()" class="dropbtn">Menu</button>
          <nav>
            <div id="myDropdown" class="dropdown-content" >
              <a href="login.php">Login</a>
              <a href="index.php">Shoe list</a>
              <a href="shop.php">Gallary</a>
            </div>
          </nav>  
          
        </div>
        

    <?php } ?>
    
    <?php if ( $_SESSION["loggedin"] ) { ?>

       <div class="dropdown">
        
          <button onclick="myFunction()" class="dropbtn">Menu</button>
          <nav>
            <div id="myDropdown" class="dropdown-content">
              <a href="logout.php">Logout</a>
              <a href="index.php">Shoe list</a>
              <a href="shop.php">Gallary</a>
            </div>
          </nav>  
          
        </div>
        

    <?php } ?>
    
    
  </header>
  
    <h1>Documentation</h1>
    <div>
        <p>This website will help you keep track of your shoes. It will keep track of the shoe, value, color and condition of the shoe. As well as a page for the user to be taken directly to some shoe sites to buy shoes.</p>
        <p>The user will have to login to add records to the database but they can access the gallary page without needing to login.The username is: Username and password is : password.</p>
        
    </div>
    <div>
       <h2>Images used</h2>
       <ul class="row list-unstyled" >
        
        <li class="col-12 col-md-6 col-lg-4"><a data-flickr-embed="true"  href="https://www.flickr.com/photos/hyku/379995118/in/photolist-acDGe-9BfvkP-zzzdx-zzzr4-zzziY-9h1kDH-zzz6x-4MMb8y-4JTd2m-hFVWQ-cjoSJ5-cekgaN-g7m4AV-FKF5m-bWXUc2-5xwHkk-bWXUn2-cekg1G-bWXUjv-cjoR5U-bb7dAF-dF4uns-cekhi5-9fF5ii-6vpyWx-b1vTyt-bWXUeF-cekgdS-o7L9fK-aVGkR4-SiEVYW-8PZ6rk-6ntZkB-8Q3hjS-66pYBC-abNoSh-8PZ7wR-ezxdpU-8Q3gDw-XMAtun-abKyYK-8PZaMP-8Q3egm-8Q3f47-bC2Ukr-evGZSG-evDRvr-26H3Z69-613sYb-dXsVws" title="Nike Store"><img src="https://live.staticflickr.com/154/379995118_6f7a8d3ae9_m.jpg" width="240" height="160" alt="Nike Store"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script></li>
        <li class="col-12 col-md-6 col-lg-4"><a data-flickr-embed="true"  href="https://www.flickr.com/photos/ccshow/177633876/in/photolist-bsNhfL-oPVXE-6VQxan-qTuLXw-9xYAEm-gGqn3-rb4Edn-9ET7p4-rq9axz-dF3hKM-LH8h3G-dVhNJK-2agoTWX-LcKYjR-NgB54z-2gFdMqV" title="WB_jumpman_1280"><img src="https://live.staticflickr.com/53/177633876_b37c8a7ec3_m.jpg" width="240" height="160" alt="WB_jumpman_1280"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script></li>
        <li class="col-12 col-md-6 col-lg-4"><a data-flickr-embed="true"  href="https://www.flickr.com/photos/victoriawhite/5578548029/in/photolist-9uXvYz-LJHw4-3vL3Y-5gWSJF-6mMjRp-55WPx-9aVLko-86AzVi-8Fw9aw-pisXP-bTvnPi-6qaLmX-9NMoCt-6M7PNH-8i2mPj-7cRbGq-azinFC-6sBpPP-dqnNS-bvqxR-2vKsFM-69tUFb-DULrP-23MHh-5hqjEr-6DUG9-269u14w-4NUDm-b4Adwc-bzMECB-3kUW3d-axQbqr-b4eHXK-nJHdN2-8feHox-xhJkk-7AZ2W-7AZ4k-8fhZzE-fNoJRY-6726oL-aeuRxt-ZWteib-5ZzxiV-azincs-azioxs-9hWuMy-66WRxT-aziiLw-azfFEn" title="adidas"><img src="https://live.staticflickr.com/5150/5578548029_17ecb631b0_m.jpg" width="240" height="160" alt="adidas"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script></li>
               
        <li class="col-12 col-md-6 col-lg-4"><a data-flickr-embed="true"  href="https://www.flickr.com/photos/mujitra/6621114137/in/photolist-b65Wyi-qfU86G-g9iNQK-8Whr1g-8NExxX-cfcWpE-9uU3dT-f1ypyi-oBzTYy-qHX3RW-qg3Jan-fEWAAD-2bkNN2S-dDkYJd-ow2pxQ-dCWug6-dtAjsr-fUZHdQ-qWCEUn-abThTB-2jYDxZ-qHWxNA-5GDwpR-pz5T9C-BZDiAg-9uxRNP-4U7fYN-coeTfL-qggCYA-oqjVdu-9uxScn-oVqD1y-8xNQCc-ecShfz-rhTKGF-fFecJy-5KdREM-qf7vrs-coeFF7-qsLDiC-qEdxnQ-8oQuvM-pow8U4-qujnRY-qdaRxn-JHEnuo-bADTGP-qvEbss-coeKZU-99xYtf" title="new balance."><img src="https://live.staticflickr.com/7141/6621114137_08ea946566_m.jpg" width="240" height="160" alt="new balance."></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script></li>
        <li class="col-12 col-md-6 col-lg-4"><a data-flickr-embed="true"  href="https://www.flickr.com/photos/hyku/380014545/in/photolist-6vZQB5-5UPS4v-4nGEj-zzF5V-zzEX7-7ubRnw-7u7Lnx-7u7Aig-9ZvphF-7ubPpC-FYYES-7ubNbu-7ufTiH-7ugiit-4feAQ-7ZfCW9-7u7LWv-6GUS5W-7ufHcK-7ugj1B-zzFeE-oE3sjL-52JH1-6FSin-qj8as-7u7B5g-4vLL2U-7ug13x-7oszV5-7osymq-pxnsge-8sR3g2-7orpem-8nFZFG-9XvKwr-5vHQkv-2zcFLL-8q9VXz-5MNFpf-ByeLw-acGkCS-5maXB3-4xrx79-6DrNne-uaeXa-6Yqa1E-FYYvf-7ujgj7-b2THkV" title="Puma Store"><img src="https://live.staticflickr.com/176/380014545_6d0d183fec_m.jpg" width="240" height="160" alt="Puma Store"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script></li>
        <li class="col-12 col-md-6 col-lg-4"><a data-flickr-embed="true"  href="https://www.flickr.com/photos/andrewmalone/2365290658/in/photolist-4B1Jvq-8UH4LD-5kNdVp-53GURC-4Ae963-53R2xe-fAdbNr-7s9CNZ-FYJV5-9C3xpz-g7D4PZ-ooHUiZ-2UN2DL-bRLwGX-8Pr92F-2BhNeK-bHFBBk-4H4idF-9FMV2a-tgGkJ-bcXkqa-55H4SS-9Q1UTn-Gynqb-uHUXS-83fVZj-75ADAZ-68AAjd-34aSZ6-uL667-4cAyTC-ao5Npj-5kStJ7-uM5Re-uM4nz-8xmYig-6kuKpB-LPmbZ-65WSPH-a8gX2F-aiL6hx-cgwVWJ-4unpRq-9g6mbD-4F5bJq-dxZyPh-3QiE9-nXPTJ8-6DiXjn-8zSwmx" title="Converse clock"><img src="https://live.staticflickr.com/2158/2365290658_4e1624f9d1_m.jpg" width="240" height="160" alt="Converse clock"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script></li>
      </ul>
    </div>
    <div>
      <h2>HTML and CSS Validation</h2>
      <hr>
      <ul class="row list-unstyled" >
        
        <li class="col-12 "><figure><figcaption><h3>Documentation page</h3></figcaption><img src="Documentation.PNG"  width="700" height="700" alt="Documentation validation"></img></figure></li>
        <hr>
        <li class="col-12 "><figure><figcaption><h3>Shoe List page</h3></figcaption><img src="index.PNG"  width="900" height="700" alt="index validation"></img></figure></li>
        <hr>
        <li class="col-12" ><figure><figcaption><h3>Gallary page</h3></figcaption><img src="Gallary.PNG" width="900" height="700" alt="Gallary validation"></img></figure></li>
        <hr>
        <li class="col-12" ><figure><figcaption><h3>Css page</h3></figcaption><img src="menuCSS.PNG" width="900" height="700" alt="CSS validation"></img></figure></li>
       </ul>
    </div>

   

    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    <script src = "menu.js"></script>
   
  </body>
</html>
