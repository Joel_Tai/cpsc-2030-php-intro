<?php
 session_start();
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel = "stylesheet" href="menu.css">
    <title>Find Shoes</title>
  </head>
     
   <body class="container">
    
    
     <header>
         <?php if ( $_SESSION["loggedin"]==false ) { ?>
     
       <div class="dropdown">
        
          <button onclick="myFunction()" class="dropbtn">Menu</button>
          <nav>
            <div id="myDropdown" class="dropdown-content" >
              <a href="login.php">Login</a>
              <a href="index.php">Shoe list</a>
              <a href="documentation.php">Documentation</a>
            </div>
          </nav>  
          
        </div>
        

    <?php } ?>
    
    <?php if ( $_SESSION["loggedin"] ) { ?>

       <div class="dropdown">
        
          <button onclick="myFunction()" class="dropbtn">Menu</button>
          <nav>
            <div id="myDropdown" class="dropdown-content">
              <a href="logout.php">Logout</a>
              <a href="index.php">Shoe list</a>
              <a href="documentation.php">Documentation</a>
            </div>
          </nav>  
          
        </div>
        

    <?php } ?>
     
         
 </header>
   
 
 
  
    <h1>Find Some New Shoes</h1>
     
     
     <main class = "container">
      <ul class="row list-unstyled" >
        
       

       
        
               
        
        <li class="col-12 col-md-6 col-lg-4"> <figure><a data-flickr-embed="true"  href="https://www.nike.com/ca/" title="nike."><img src="https://live.staticflickr.com/154/379995118_6f7a8d3ae9_m.jpg" width="240" height="160" alt="nike logo"></a><figcaption>Nike</figcaption></figure></li>
        <li class="col-12 col-md-6 col-lg-4"> <figure><a data-flickr-embed="true"  href="https://www.nike.com/ca/en_gb/c/jordan" title="jordan."><img src="https://live.staticflickr.com/53/177633876_b37c8a7ec3_m.jpg" width="240" height="160" alt="jumpman logo."></a><figcaption>Jordan</figcaption></figure></li>
        <li class="col-12 col-md-6 col-lg-4"> <figure><a data-flickr-embed="true"  href="https://www.adidas.ca/en/" title="adidas."><img src="https://live.staticflickr.com/5150/5578548029_17ecb631b0_m.jpg" width="240" height="160" alt="adidas logo"></a><figcaption>Adidas</figcaption></figure></li>
        
        
        <li class="col-12 col-md-6 col-lg-4"> <figure><a data-flickr-embed="true"  href="https://www.newbalance.ca/" title="new balance"><img src="https://live.staticflickr.com/7141/6621114137_08ea946566_m.jpg" width="240" height="160" alt="new balance logo"></a><figcaption>New Balance</figcaption></figure></li>
        <li class="col-12 col-md-6 col-lg-4"> <figure><a data-flickr-embed="true"  href="https://ca.puma.com/" title="puma"><img src="https://live.staticflickr.com/176/380014545_6d0d183fec_m.jpg" width="240" height="160" alt="Puma logo"></a><figcaption>Puma</figcaption></figure></li>
        <li class="col-12 col-md-6 col-lg-4"> <figure><a data-flickr-embed="true"  href="https://www.converse.ca/" title="converse"><img src="https://live.staticflickr.com/2158/2365290658_4e1624f9d1_m.jpg" width="240" height="160" alt="converse logo"></a><figcaption>Converse</figcaption></figure></li>
        
      </ul>
    </main>


   


    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src = "menu.js"></script>
   
   
  </body>
</html>