<?php
 session_start();
?>

<!DOCTYPE html>
<html>
    <head>
         <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <h1>Log Out</h1>
    </head>
    <body>
        <p>You have been logged out.</p>
        <a href="index.php">return to the list page</a><br>
        <a href="documentation.php">return to the documentation page</a><br>
        <a href="shop.php">return to the shop page</a><br>
    </body>
</html>
<?php
$_SESSION = array();
session_destroy();

?>