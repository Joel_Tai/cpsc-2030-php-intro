<!DOCTYPE html>
<html>
    <style>
        footer
        {
            position: absolute;
            bottom: 0;
        }
        body
        {
            padding:10px;
        }
        
    </style>
    <body>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <h1>Check Out</h1>
        <hr class="mb-4">
       <div class = "row">
            <div class = "col-12 col-md-6">
                <h2>Information</h2>
                <b>Name:</b> <?php echo $_POST["firstName"]," ",$_POST["lastName"] ; ?><br>
                <b>Email:</b> 
                <?php
                    if( $_POST["email"]!=null)
                    {
                        echo $_POST["email"];   
                    }
                    else
                    {
                        echo "No email entered";
                    }
                ;?><br>
                <b>Address:</b> <?php echo $_POST["address"];?><br>
                <b>Address 2:</b>
                <?php if($_POST["address2"] != null)
                    {
                        echo $_POST["address"];
                    }
                    else
                    {
                        echo "No second address";
                    }
               ;?><br>
               <b>Country:</b> <?php echo $_POST["country"]  ;?><br>
               <b>Province/Territory</b>: <?php echo $_POST["province"] ;?><br>
               <b>Postal Code:</b> <?php echo $_POST["postal"]  ;?><br>
            </div>
            <div class = "col-12 col-md-6">
                <h2>Order</h2>
                <b>Cups:</b> <?php echo $_POST["Cup"];?><br>
                <b>Plates</b>: <?php echo $_POST["Plate"];?><br>
                <b>Forks:</b> <?php echo $_POST["Fork"];?><br>    
                <b>Gift Wrap:</b> 
                <?php if($_POST["gift"] == true)
                    {
                        echo "Yes";
                    }
                    else
                    {
                        echo "No";
                    }
               ;?><br>
               <b>Delivery:</b> <?php echo $_POST["deliveryMethod"];?><br>
               <b>Total: $</b>
               <?php
                    $gift;
                    $total = (12*$_POST["Cup"])+(8*$_POST["Plate"])+(5*$_POST["Fork"]);
                    $delivery;
                    if($_POST["gift"] == true)
                    {
                        $gift = 5;
                    }
                    else
                    {
                        $gift = 0;
                    }
                    switch($_POST["deliveryMethod"])
                    {
                        case "mail":
                            $delivery = 3;
                            break;
                        case "courier":
                            $delivery = 2;
                            break;
                        case "pickup":
                            $delivery = 1;
                            break;
                        default:
                            $delivery=0;
                            break;
                    }
                    $total = $gift+$total+$delivery;
                    echo $total;
                    
               ?><br>
               
               <?php
                    $Ctax;
                    $Ptax;
                    $tax;
                    $grandTotal; 
                    switch($_POST["province"])
                    {
                        case "alberta":
                            $tax = .05;
                            break;
                        case "British Columbia":
                            $tax = .12;
                            break;
                        case "Manitoba":
                            $tax = .13;
                            break;
                        case "New-Brunswick":
                            $tax = .15;
                            break;
                        case "Newfoundland and Labrador":
                            $tax = .15;
                            break;
                        case "Northwest Territories":
                            $tax = .05;
                            break;
                        case "Nova Scotia":
                            $tax = .15;
                            break;
                        case "Nunavut":
                            $tax = .05;
                            break;
                        case "Ontario":
                            $tax = .13;
                            break;
                        case "Prince Edward Island":
                            $tax = .15;
                            break;
                        case "Quebec":
                            $tax = .14975;
                            break;
                        case "Saskatchewan":
                            $tax = .11;
                            break;
                        case "":
                            $tax = .05;
                            break;
                    }
                    $taxOut = $tax*$total;
                    
                ?>
                <b>Tax: $</b><?php echo $taxOut?><br>
                <b>Grand Total: </b>
                <?php
                    $grandTotal = $taxOut+$total;
                    if($grandTotal>750)
                    {
                        echo "<p style='color:red;'>"."Your card has been declined, your total was over $750"."</p>";
                    }
                    else
                    {
                        
                        echo "$ ".$grandTotal;
                    }
                    
                ?><br>
                
            </div>
        </div>
        
        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>